<?php

namespace FileTransfer;

/**
 * Адаптер подключения по протоколу SFTP.
 * Расширяет родительский класс file_transfer методом connect();
 * 
 *
 * @author jerry
 */
class SFTPAdapter extends file_transfer {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Подключение к хосту по протоколу SFTP
     * 
     * @param string $host
     * @param bool/integer $port
     * @return $this
     * @throws \Exception
     */
    public function connect($host, $port = 22) {
        $connection = ftp_ssl_connect($host, $port);


        if (false === $connection) {
            throw new \Exception('Unable to connect');
        }

        /* В родительском классе устанавливаем параметр текущего подключения */
        $this->setConnection($connection);

        return $this;
    }

}
