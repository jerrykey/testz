<?php

namespace FileTransfer;

/**
 * FileTransfer Interface
 * 
 * Интерфейс класса.
 * Чтобы не забыть определить какие-то методы.
 * Написан исходя из вводных данных тестового задания.
 * 
 *  @package FileTransfer
 * 
 *  @author jerry
 */
interface file_transfer_interface {
    
    
    public function login($user, $passowrd);
    
    public function cd($path);
    
    public function download($filename);
    
    public function close();
    
    public function pwd();
    
    public function upload($filename);
    
    public function exec($command);
    
    public function nlist();
    
    
}
