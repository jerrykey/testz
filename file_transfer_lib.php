<?php

namespace FileTransfer;

use Exception;
use ErrorException;


/**
 * Класс Factory реализует паттерн программирования Factory.
 * В зависимости от протокола подключения использует различные методы.
 *
 * @author jerry
 */
class Factory {

    public function getConnection($protocol, $user, $passowrd, $host, $port = FALSE) {
        switch ($protocol) {
            case 'ftp':
                require_once 'ftp_adapter.php';

                $f = new FTPAdapter();

                break;
            case 'ssh':
                require_once 'ssh_adapter.php';

                $f = new SFTPAdapter();

                break;
            default :
                throw new \Exception('Protocol is not supported');
        }

        try {
            $f->connect($host, $port)
                    ->login($user, $passowrd);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }

        return $f;
    }

}
