<?php

namespace FileTransfer;

require './file_transfer_interface.php';

/**
 * Class file_transfer
 * 
 * @package FileTransfer
 * 
 * Класс file_transfer.
 * Предоставляет общие методы по работе с удаленными файлами.
 *
 * @author jerry
 */
class file_transfer implements file_transfer_interface {

    /**
     * FTP strem
     * @var FTP stream 
     */
    protected $connection;

    /**
     * Current remote directory
     * @var string 
     */
    protected $curDir;

    public function __construct() {
        
    }

    /**
     * Сеттер параметра connection.
     * Метод используется для изменения параметра из дочернего класса.
     * 
     * @param FTP stream $connection
     */
    public function setConnection($connection) {
        $this->connection = $connection;
    }

    /**
     * Геттер параметра connection.
     * 
     * @return FTP stream $connection
     */
    public function getConnection() {
        return $this->connection;
    }

    /**
     * Метод-оболочка функции ftp_login
     * Авторизуется на удаленном хосте
     * 
     * @param string $user
     * @param string $passowrd
     * @return bool Connection Status
     */
    public function login($user, $passowrd) {
        $loggedIn = ftp_login($this->connection, $user, $passowrd);

        if (true !== $loggedIn) {
            throw new \Exception('Unable to log in');
        }

        return $this;
    }

    /**
     * Метод смены директории на удаленном хосте.
     * Меняет текущую директорию или выбрасывает эксепшен.
     * 
     * @param string $path
     * @return $this
     * @throws Exception
     */
    public function cd($path) {
        try {
            $changeDir = ftp_chdir($this->connection, $path);
            if (true !== $changeDir) {
                throw new \Exception('Can\'t Change DIR');
            }
            $this->curDir = $path;
        } catch (\Exception $exc) {
            var_dump($exc->getMessage());
        }

        return $this;
    }

    /**
     * Метод загрузки удаленного файла в рабочую директорию.
     * 
     * @param string $filename
     * @return $this
     * @throws Exception
     */
    public function download($filename) {
        /* Входим в пассивный режим */
        ftp_pasv($this->connection, true);

        try {
            $downloadFilename = ftp_get($this->connection, __DIR__ . '/' . $filename, $filename, FTP_BINARY, 0);
            if (true !== $downloadFilename) {
                throw new \Exception('Can\'t download Filename');
            }
        } catch (Exception $exc) {
            var_dump($exc->getMessage());
        }


        return $this;
    }

    /**
     * Метод закрывает текущее соединение.
     * @throws Exception
     */
    public function close() {
        try {
            $close = ftp_close($this->connection);
            if (true !== $close) {
                throw new \Exception('Can\'t close connection');
            }
        } catch (Exception $exc) {
            var_dump($exc->getMessage());
        }
    }

    /**
     * Метод оболочка функции ftp_pwd
     * Возвращает название текущей директории
     * 
     * @return string
     */
    public function pwd() {
        return ftp_pwd($this->connection);
    }

    /**
     * Метод выгрузки файла из рабочей директории в текущую удаленную директорию
     * 
     * @param string $filename
     * @return $this
     * @throws Exception
     */
    public function upload($filename) {
        /* Входим в пассивный режим */
        ftp_pasv($this->connection, true);

        try {
            $uploadFilename = ftp_put($this->connection, $filename, __DIR__ . '/' . $filename, FTP_BINARY, 0);
            if (true !== $uploadFilename) {
                throw new \Exception('Can\'t upload Filename');
            }
        } catch (Exception $exc) {
            var_dump($exc->getMessage());
        }


        return $this;
    }

    /**
     * Метод выполнения FTP команды на удаленном хосте.
     * Важно учитывать, то что не все комманды поддерживаются всеми FTP серверами.
     * 
     * @param string $command
     */
    public function exec($command) {
        try {
            ftp_exec($this->connection, $command);
        } catch (\Exception $exc) {
            var_dump($exc->getMessage());
        }
    }

    /**
     * Метод возвращает массив с переченем файлов на удаленном хосте.
     * 
     * @return array
     * @throws Exception
     */
    public function nlist() {
        try {
            $list = ftp_nlist($this->connection, $this->curDir);
            if (FALSE === $list) {
                throw new \Exception('Can\'t list DIR');
            }
        } catch (\Exception $exc) {
            var_dump($exc->getMessage());
        }
        return $list;
    }

}
