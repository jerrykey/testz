<?php

namespace FileTransfer;

require_once 'file_transfer.php';

/**
 * Адаптер подключения по протоколу FTP.
 * Расширяет родительский класс file_transfer методом connect();
 * 
 *
 * @author jerry
 */
class FTPAdapter extends file_transfer {


    public function __construct() {
        parent::__construct();
    }

    /**
     * Подключение к хосту по протоколу FTP
     * 
     * @param string $host
     * @param bool/integer $port
     * @return $this
     * @throws \Exception
     */
    public function connect($host, $port = 21) {
        $connection = ftp_connect($host, $port);


        if (false === $connection) {
            throw new \Exception('Unable to connect');
        }
        
        /* В родительском классе устанавливаем параметр текущего подключения */
        $this->setConnection($connection);

        return $this;
    }

}
