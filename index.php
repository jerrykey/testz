<?php

include 'file_transfer_lib.php';

use FileTransfer as FT;

$factory = new FT\Factory();

try {
    $conn = $factory->getConnection('ssh', 'user', 'pass', 'hostname.com', 2222);
    $conn->cd('/var/www')
            ->download('dump.tar.gz')
            ->close();
} catch (Exception $e) {
    echo $e->getMessage();
}
try {
    $conn = $factory->getConnection('ftp', 'user', 'pass', 'hostname.com');
    echo $conn->pwd() . "\n";
    $conn->upload('archive.zip');
    // Замечание 
    // Как я понимаю, задумывалось, что конструкция $conn->exec('ls -al') должна вывести перечень файлов на удаленном хосте.
    // Но, функция ftp_exec возвращает лишь bool - TRUE или FALSE и вывести перечень файлов не получится
    // А также, не все FTP хосты поддерживают комманду 'ls -al'
    print_r($conn->exec('ls -al'));
    // Для выода массива с перечнем файлов в удаленной директории лучше использовать метод nlist
    // print_r($conn->nlist()); 
} catch (Exception $e) {
    echo $e->getMessage();
}
